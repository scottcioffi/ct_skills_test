<?php
// CT_Theme Customizations

function ct_theme_settings_menu() {

	//create new top-level menu
	add_menu_page('ct_custom Settings', 'CT_Custom Theme Settings', 'administrator', __FILE__, 'ct_options_page' , 'dashicons-admin-generic' );
}
add_action('admin_menu', 'ct_theme_settings_menu');

function register_ct_theme_settings() {
	//register our settings
	register_setting( 'CT_Theme_Options', 'logo', 'handle_logo_upload');
	register_setting( 'CT_Theme_Options', 'facebook' );
	register_setting( 'CT_Theme_Options', 'linkedin' );
    register_setting( 'CT_Theme_Options', 'pinterest' );
    register_setting( 'CT_Theme_Options', 'twitter' );
    register_setting( 'CT_Theme_Options', 'logo' );
}
add_action( 'admin_init', 'register_ct_theme_settings' );

function handle_logo_upload($option){
  if(!empty($_FILES["logo"]["tmp_name"]))
  {
    $urls = wp_handle_upload($_FILES["logo"], array('test_form' => FALSE));
    $temp = $urls["url"];
    return $temp;  
  }
  $option=esc_attr( get_option('logo') );
  return $option;
}

function ct_options_page() {
?>
<div class="wrap">
<h1>Customize Theme: CT_Theme</h1>

<form method="post" id="themeoptions" action="options.php" enctype="multipart/form-data">
    <?php 
        settings_fields( 'CT_Theme_Options' ); 
        do_settings_sections( 'CT_Theme_Options' ); 
    ?>
        <?php 
        if(!empty($_FILES["logo"]["tmp_name"])){
            $attachment_id = media_handle_upload( 'logo', 0 );
            if ( is_wp_error( $attachment_id ) ) {
                $error_string = $attachment_id->get_error_message();
                print_r(get_error_message());
                echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
                // There was an error uploading the image.
            } else {
                echo $attachment_id;
                print_r($attachment_id);
                // The image was uploaded successfully!
            }
        }
        if((get_option('logo'))!==''){
            echo '<img id="ct_custom_logo" src="'.esc_attr( get_option('logo') ).'">';
        }
        else{
            the_custom_logo();  
        }
        ?>

<label>Upload Your Logo Here
<input type="file" accept=".jpg,.gif,.jpeg,.png" name="logo" value="<?php echo esc_attr(get_option('logo')); ?>" /></label>

<label>Pinterest URL
<input type="text" name="pinterest" value="<?php echo esc_attr( get_option('pinterest') ); ?>" /><label>
         
<label>Facebook URL
<input type="text" name="facebook" value="<?php echo esc_attr( get_option('facebook') ); ?>" /></label>
  
        
<label>LinkedIn URL
<input type="text" name="linkedin" value="<?php echo esc_attr( get_option('linkedin') ); ?>" /></label>

<label>Twitter URL
<input type="text" name="twitter" value="<?php echo esc_attr( get_option('twitter') ); ?>" /></label>


    <?php submit_button(); ?>

</form>
</div>
<?php } ?>