<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oEbvVmrahI8ash/5geTJqLAGgtI7sWM+FH9bRC07DYaTHFPY/j//CMW4XfXNdsV6/AJAaoox/WvWSxTl08YD2A==');
define('SECURE_AUTH_KEY',  'SCl+B+s6P4ZvpUVTRHnoqiX0SPAQD1yIA6ZHatTVwaAtVLOYVVVqF7lLpcoItm5ovp6tRjFHZw0Y+xP4T5PrwQ==');
define('LOGGED_IN_KEY',    'CHm2occ6SX7KkLdVoypYqH98Jw0YzThdNp47M3/qMKSRqtABmCOReZ4XGuA0FTk3WBQgdpGml55iUqZ9xMyIZQ==');
define('NONCE_KEY',        'X7PB3MrpEQBbQeqAuUnUTPpKNXNpmellm3ArPdiv2bwwvhtUOIr0+KOm6NEbGIfP0txz4/rWOhJej39Nsi4q0A==');
define('AUTH_SALT',        'UEliLR7+FmGFHNliTapCGKz86vXxArtDtjYiIQyvZ/uPWcoeHSD9SB2H6jbIG1JmzFt/eLtwskiBa+Bb2vtpgg==');
define('SECURE_AUTH_SALT', 'fIdR9rb8Md0MSyWHWbC1flXTmHgcsjD06wyMr3TGAfp+em/GCMCJtlGD6RMHQv4CZHPi8S9xlxW/jM3s4k2PkA==');
define('LOGGED_IN_SALT',   'v8koglD1wA3QgC2UNpYCetwWw9BzqfFykF6GVRwVdZXP7pbZrAkhIu49MtTMgDZ/ddQiYodpFmnlVvlgI5aoxQ==');
define('NONCE_SALT',       'PJQG9r7Pc3e48/DBClW5hPn7BCZKs6GaiNhwyKG8bj4KelqZckDCYBiKtJzKcPqe/zWsEwPY+rFhukff3FR+CQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
