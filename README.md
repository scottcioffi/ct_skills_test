# Skills Test #
__Applicant:__ Scott Cioffi  
__Email Address:__ scott.cioffi@gmail.com

This repository is a skilltest demonstration written by Scott Cioffi for a job position as Web Developer. Scott was given the following image:
![PSD Skills Test Mockup](/skillsTestMockup.jpg?raw=true "Title")

## Client Request: ##

In this skill test, you’ll be taking a converted PSD to HTML file and incorporating that into a WordPress template.

You’ll be responsible for converting a design into a WordPress template. Here are the task details

1.  Please create a vhost in your local development environment with hostname set as: coalitiontest.local
2.  Download latest Wordpress and install in your local site. (i.e. site url and home url should be http://coalitiontest.local )
3.  Please keep username coalitiontest and password wordpresstest
4.  Once the installation is completed, please download this zip file.
5.  This zip file contains the base theme for WP (underscores) and PSD design file. Please activate that theme and incorporate the PSD design into the WP template.
6.  Create a new template with name homepage.php
7.  Create a new page with title “Homepage” and assign homepage.php template to this page. This page should also be marked as your frontpage.
8.  Please create a theme settings page (without using any plugin). Theme settings should include the following:
	-  Image Upload for Logo
	-  Phone Number
	-  Address Information
	-  Fax Number
	-  Social Media Links
9. Menu should be created within WordPress
10. You can use Contact Form 7 for the form
11. Page content should be manageable within Wordpress.